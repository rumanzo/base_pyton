#!/usr/bin/python3.8

# 7. Реализовать проект «Операции с комплексными числами». Создайте класс «Комплексное число»,
# реализуйте перегрузку методов сложения и умножения комплексных чисел.
# Проверьте работу проекта, создав экземпляры класса (комплексные числа) и выполнив сложение и
# умножение созданных экземпляров. Проверьте корректность полученного результата.

class ComplexNumber:

    def __init__(self, a: int, b: int):
        self.a = a
        self.b = b

    def __add__(self, b: 'ComplexNumber'):
        return ComplexNumber(self.a + b.a, self.b + b.b)

    def __mul__(self, b: 'ComplexNumber'):
        return ComplexNumber(self.a*b.a - self.b*b.b, self.a*b.b + self.b*b.a)

    def __str__(self):
        if self.b >= 0:
            return f'{self.a} + {self.b}i'
        else:
            return f'{self.a} {self.b}i'

test = ComplexNumber(1,2)
print(test + ComplexNumber(1,5))
print(test * ComplexNumber(1,5))
