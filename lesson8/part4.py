#!/usr/bin/python3.8
# 4. Начните работу над проектом «Склад оргтехники». Создайте класс, описывающий склад.
# А также класс «Оргтехника», который будет базовым для классов-наследников.
# Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс).
# В базовом классе определить параметры, общие для приведенных типов.
# В классах-наследниках реализовать параметры, уникальные для каждого типа оргтехники.


class Stock:
    name: str
    serial: str
    cost: int

    def __init__(self, name, serial, cost):
        self.name = name
        self.serial = serial
        self.cost = cost

class OfficeEquipment(Stock):
    connecttype: str #ltp, usb, network, etc.
    def __init__(self, name, serial, cost, connecttype):
        super().__init__(name, serial, cost)
        self.connecttype = connecttype

class Printer(OfficeEquipment):
    sheetcount: int
    printspeed: int # sheet per min
    printertype: str # laser, matrix, etc

    def __init__(self, name, serial, cost, connecttype, sheetcount, printspeed, printertype):
        super().__init__(name, serial, cost, connecttype)
        self.sheetcount = sheetcount
        self.printspeed = printspeed
        self.printertype = printertype

class Scaner(OfficeEquipment):
    dpi: int
    scansize: str

    def __init__(self, name, serial, cost, connecttype, dpi, scansize):
        super().__init__(name, serial, cost, connecttype)
        self.dpi = dpi
        self.scansize = scansize

class Copier(Printer, Scaner):

    def __init__(self,  name, serial, cost, connecttype, sheetcount, printspeed, printertype, dpi, scansize):
        self.name = name
        self.serial = serial
        self.cost = cost
        self.connecttype = connecttype
        self.sheetcount = sheetcount
        self.printspeed = printspeed
        self.printertype = printertype
        self.dpi = dpi
        self.scansize = scansize

    # P.S. не понял как наследовать init разных классов через super, если указаны 2 и более



test = Copier(name='test', serial='test',cost=555, connecttype='usb', sheetcount=444, printspeed=4444, printertype='test',
              dpi= 444, scansize='50x50')
print(test.name)