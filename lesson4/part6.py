#!/usr/bin/python3.8
# 6. Реализовать два небольших скрипта:
# а) итератор, генерирующий целые числа, начиная с указанного,
# б) итератор, повторяющий элементы некоторого списка, определенного заранее.
#
# Подсказка: использовать функцию count() и cycle() модуля itertools. Обратите внимание, что создаваемый
# цикл не должен быть бесконечным. Необходимо предусмотреть условие его завершения.

from itertools import count, cycle
from typing import Iterator


def a(start: int) -> Iterator:
    """
    итератор, генерирующий целые числа, начиная с указанного
    :param start:  начальное число
    :return: итератор
    """
    for num in count(start):
        if num - start > 10:
            break
        yield num


for x in a(10):
    print(x)


def b(elems: list) -> Iterator:
    """
    итератор, повторяющий элементы некоторого списка, определенного заранее.
    :param elems: список
    :return: итератор
    """
    n = 0
    for elem in cycle(elems):
        if n > 10:
            break
        n += 1
        yield elem


for x in b([4, 8, 15, 16, 23, 42]):
    print(x)
