#!/usr/bin/python3.8
# 1. Реализовать класс «Дата», функция-конструктор которого должна принимать дату в виде строки формата «день-месяц-год».
# В рамках класса реализовать два метода. Первый, с декоратором @classmethod, должен извлекать число, месяц, год и
# преобразовывать их тип к типу «Число». Второй, с декоратором @staticmethod, должен проводить валидацию числа, месяца и
# года (например, месяц — от 1 до 12). Проверить работу полученной структуры на реальных данных.


class Date:
    date: str
    dateint: int

    def __init__(self, date):
        self.date = date

    @classmethod
    def from_string(cls, date_as_string):
        if cls.is_date_valid(date_as_string) is False:
            assert 'Неверный формат даты. Используйте формат %d-%m-%Y'
        day, month, year = date_as_string.split('-')
        return int(day + month + year)

    @staticmethod
    def is_date_valid(date_as_string: str):
        day, month, year = map(int, date_as_string.split('-'))
        return 0 < day <= 31 and 0 < month <= 31 and 0 < year <= 9999


print(Date.is_date_valid('15-06-2001'))
obj = Date.from_string('15-06-2001')
print(obj)
print(type(obj))

# P.S. вернуть экземпляр объекта с classmethod это понятно, но зачем использовать classmethod и возвращать int (по условиям задачи)?
