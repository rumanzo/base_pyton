#!/usr/bin/python3.8

# 2. Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль.
# Проверьте его работу на данных, вводимых пользователем. При вводе пользователем нуля в качестве делителя программа
# должна корректно обработать эту ситуацию и не завершиться с ошибкой.


class Safedivision(int):

    def __truediv__(self, num):
        if num == 0:
            return False
        else:
            return self.numerator / num


while True:
    numerator = Safedivision(input('Введите делимое: '))
    denominator = Safedivision(input('Введите делитель: '))
    print(f'Результат: {numerator / denominator}')
