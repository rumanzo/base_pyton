#!/usr/bin/python3.8

# 2. Реализовать функцию, принимающую несколько параметров, описывающих данные пользователя:
# имя, фамилия, год рождения, город проживания, email, телефон. Функция должна принимать параметры как
# именованные аргументы. Реализовать вывод данных о пользователе одной строкой.

def person(name: str = None, lastname: str = None, bird_year: int = None, city: str = None, email: str = None,
           telephone: str = None) -> str:
    """
    Выводит описанные данные о пользователе
    :param name: имя
    :param lastname: фамилия
    :param bird_year: год рождения
    :param city: город проживания
    :param email: электронная почта
    :param telephone: телефон
    :return: сериализованная строка
    """
    args = locals()
    return str({k: v for k, v in args.items() if v is not None})


print(person(name='John', lastname='Doe', bird_year=1892))
