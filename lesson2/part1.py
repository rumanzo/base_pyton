#!/usr/bin/python3
# 1. Создать список и заполнить его элементами различных типов данных.
# Реализовать скрипт проверки типа данных каждого элемента. Использовать функцию type() для проверки типа.
# Элементы списка можно не запрашивать у пользователя, а указать явно, в программе.

l = [1, "2", True, {1: 1}, (1,2), [3,4], set('some'), frozenset('some')]

for entry in l:
    print(entry, 'is', type(entry))