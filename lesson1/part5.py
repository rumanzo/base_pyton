#!/usr/bin/python3
# 5. Запросите у пользователя значения выручки и издержек фирмы.
# Определите, с каким финансовым результатом работает фирма (прибыль — выручка больше издержек, или убыток — издержки больше выручки).
# Выведите соответствующее сообщение. Если фирма отработала с прибылью, вычислите рентабельность выручки (соотношение прибыли к выручке).
# Далее запросите численность сотрудников фирмы и определите прибыль фирмы в расчете на одного сотрудника.

profit = int(input("Введите прибыль фирмы: "))
expenses = int(input("Введите издержки фирмы: "))

if profit > expenses:
    print("Фирма приносит доход")
    # Формула рентабельности выручки:  (Чистая прибыль / выручка) * 100 %
    print(f'Рентабельность выручки {(profit-expenses)/profit * 100:.2f}%')
    employees = int(input("Введите количество сотрудников: "))
    print(f'Прибыль фирмы в рассчте на одного сотрудника: {(profit-expenses)/employees:.2f} у.е.')
else:
    print("Фирма приносит убытки")