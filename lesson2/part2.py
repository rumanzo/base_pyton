#!/usr/bin/python3
# 2. Для списка реализовать обмен значений соседних элементов, т.е.
# Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
# При нечетном количестве элементов последний сохранить на своем месте.
# Для заполнения списка элементов необходимо использовать функцию input().

l = input("Введите элементы через пробел: ").split()

for index in range(1, len(l), 2):
    l[index-1], l[index] =  l[index], l[index-1]

print(l)