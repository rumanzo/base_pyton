#!/usr/bin/python3.8
# 7. Создать вручную и заполнить несколькими строками текстовый файл, в котором каждая строка должна содержать данные
# о фирме: название, форма собственности, выручка, издержки.
# Пример строки файла: firm_1 ООО 10000 5000.
#
# Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также среднюю прибыль.
# Если фирма получила убытки, в расчет средней прибыли ее не включать.
# Далее реализовать список. Он должен содержать словарь с фирмами и их прибылями, а также словарь со средней прибылью.
# Если фирма получила убытки, также добавить ее в словарь (со значением убытков).
# Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].
#
# Итоговый список сохранить в виде json-объекта в соответствующий файл.
# Пример json-объекта:
#
# [{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]

import json

with open('part7.txt', 'r', encoding='utf-8') as f:
    firms_profit = {}
    all_profits = []
    for line in f:
        firm, form, profit, costs = line.rstrip('\n').split()
        fullprofit = int(profit) - int(costs)
        firms_profit[firm] = fullprofit
        if fullprofit > 0:
            all_profits.append(fullprofit)

with open('part7_w.txt', 'w') as f:
    json.dump([firms_profit, { 'average_profit': sum(all_profits) / len(all_profits)}], f)
