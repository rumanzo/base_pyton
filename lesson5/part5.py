#!/usr/bin/python3.8
# 5. Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных пробелами.
# Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.
import random

with open('part5.txt', 'w', encoding='utf-8') as f:
    f.write(' '.join([str(random.randint(0, 1000)) for x in range(0, 10)]))

with open('part5.txt', 'r', encoding='utf-8') as f:
    print(f"Сумма из файла: {sum([int(x) for x in f.readline().split()])}")
