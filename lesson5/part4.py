#!/usr/bin/python3.8
# 4. Создать (не программно) текстовый файл со следующим содержимым:
# One — 1
# Two — 2
# Three — 3
# Four — 4
#
# Необходимо написать программу, открывающую файл на чтение и считывающую построчно данные.
# При этом английские числительные должны заменяться на русские. Новый блок строк должен записываться в новый текстовый файл.

numbers = {'One': 'Один', 'Two': 'Два', 'Three': 'Три', 'Four': 'Четыре',
           'Five': 'Пять', 'Six': 'Шесть', 'Seven': 'Семь', 'Eight': 'Восемь', 'Nine': 'Девять', 'Ten': 'Десять'}
with open('part4.txt', 'r', encoding='utf-8') as f1:
    with open('part4_w.txt', 'w', encoding='utf-8') as f2:
        for line in f1:
            words = line.rstrip('\n').split()
            f2.write(f"{numbers[words[0]]} {' '.join(words[1:])}\n")

