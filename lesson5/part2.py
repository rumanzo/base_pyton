#!/usr/bin/python3.8
# 2. Создать текстовый файл (не программно), сохранить в нем несколько строк,
# выполнить подсчет количества строк, количества слов в каждой строке.

with open('part2.txt', 'r', encoding='utf-8') as f:
    lines = f.readlines()
    word_count = [len(x.split()) for x in lines]
    for num, words in enumerate(word_count):
        print(f"В строке {num}: {words} слов")

print(f"Количество строк: {len(lines)}\nОбщее количество слов: {sum(word_count)}")
