#!/usr/bin/python3.8

# 4.Реализуйте базовый класс Car.
# У данного класса должны быть следующие атрибуты: speed, color, name, is_police (булево).
# А также методы: go, stop, turn(direction), которые должны сообщать, что машина поехала, остановилась, повернула (куда).
# Опишите несколько дочерних классов: TownCar, SportCar, WorkCar, PoliceCar. Добавьте в базовый класс метод show_speed,
# который должен показывать текущую скорость автомобиля. Для классов TownCar и WorkCar переопределите метод show_speed.
# При значении скорости свыше 60 (TownCar) и 40 (WorkCar) должно выводиться сообщение о превышении скорости.

class Car:
    speed: int
    color: str
    name: str
    is_police: bool

    def __init__(self, speed, color, name, is_police=False):
        self.speed = speed
        self.color = color
        self.name = name
        self.is_police = is_police

    def go(self):
        print(f'Машина {self.name} поехала')

    def stop(self):
        print(f'Машина {self.name} останосилась')

    def turn(self, direction):
        print(f'Машина {self.name} повернула {direction}')

    def show_speed(self):
        print(f'Машина {self.name} едет со скоростью {self.speed} км\ч')


class SportCar(Car):
    pass


class PoliceCar(Car):
    def __init__(self, speed, color, name, is_police=True):
        super().__init__(speed, color, name, is_police)


class TownCar(Car):
    def __init__(self, speed, color, name, is_police=False):
        super().__init__(speed, color, name, is_police)

    def show_speed(self):
        if self.speed <= 60:
            print(f'Машина {self.name} едет со скоростью {self.speed} км\ч')
        else:
            print(f'Машина {self.name} движется с превышением скорости: {self.speed} км\ч')


class WorkCar(Car):
    def __init__(self, speed, color, name, is_police=False):
        super().__init__(speed, color, name, is_police)

    def show_speed(self):
        if self.speed <= 40:
            print(f'Машина {self.name} едет со скоростью {self.speed} км\ч')
        else:
            print(f'Машина {self.name} движется с превышением скорости: {self.speed} км\ч')


town = TownCar(name='solaris', speed=80, color='red')
town.go()
town.show_speed()

police = PoliceCar(name='whatever', speed=80, color='black')
police.go()
police.stop()
police.go()
police.show_speed()

workcar = WorkCar(name='postcar', speed=60, color='green')
workcar.go()
workcar.show_speed()
