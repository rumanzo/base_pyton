#!/usr/bin/python3.8

# 5. Программа запрашивает у пользователя строку чисел, разделенных пробелом.
# При нажатии Enter должна выводиться сумма чисел.
# Пользователь может продолжить ввод чисел, разделенных пробелом и снова нажать Enter.
# Сумма вновь введенных чисел будет добавляться к уже подсчитанной сумме.
# Но если вместо числа вводится специальный символ, выполнение программы завершается.
# Если специальный символ введен после нескольких чисел, то вначале нужно добавить сумму этих чисел к
# полученной ранее сумме и после этого завершить программу.


x = 0

while True:
    ints = input('Введите числа, разделенные пробелом (для завершения работы введите в конце выражения ; ): ')
    if ints[-1] == ';':
        ints = ints[:-1]
        x += sum([int(x) for x in ints.split()])
        print("Результат: ", x)
        break
    else:
        x += sum([int(x) for x in ints.split()])
        print("Результат: ", x)
