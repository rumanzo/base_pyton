#!/usr/bin/python3
# 4. Пользователь вводит целое положительное число. Найдите самую большую цифру в числе.
# Для решения используйте цикл while и арифметические операции.
num = input("Введите число: ")
n = 0
maxnum = 0
while n < len(num):
    if int(num[n]) > maxnum:
        maxnum = int(num[n])
    n += 1
print(maxnum)
# P.S. надеюсь я правильно понял про арифметические операции

#or

print(max(num))
