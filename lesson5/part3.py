#!/usr/bin/python3.8
# 3. Создать текстовый файл (не программно), построчно записать фамилии сотрудников и величину их окладов (не менее 10 строк).
# Определить, кто из сотрудников имеет оклад менее 20 тыс., вывести фамилии этих сотрудников. Выполнить подсчет средней величины дохода сотрудников.
# Пример файла:
#
# Иванов 23543.12
# Петров 13749.32

with open('part3.txt', 'r', encoding='utf-8') as f:
    employee_payment = [x for x in f.readlines()]
    employee = []
    middle = []
    for line in employee_payment:
        lastname, payment = line.split()
        middle.append(float(payment))
        if float(payment) < 20000:
            employee.append(lastname)
    middle = sum(middle) / len(middle)
    print(f"Фамилии сотрудников с окладом менее 20 тыс.р.: {' '.join([x for x in employee])}")
    print(f"Средняя зарплата сотрудников: {middle:.2f}")
