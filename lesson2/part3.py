#!/usr/bin/python3
# 3. Пользователь вводит месяц в виде целого числа от 1 до 12.
# Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
# Напишите решения через list и через dict.?

# Вариант с list

month_num = int(input("Введите месяц в циферном виде: "))
if month_num not in range(1, 13):
    raise Exception('Напишите число от 1 до 12')

mounts = ['Зима', 'Весна', 'Лето', 'Осень']
mounts_dict = {0: 'Зима', 1: 'Весна', 2: 'Лето', 3: 'Осень'}

if month_num < 2 or month_num == 12:
    n = 0
elif month_num < 6:
    n = 1
elif month_num < 9:
    n = 2
else:
    n = 3
print('Текущее время года из массива: ', mounts[n])
print('Текущее время года из словаря: ', mounts_dict[n])
