#!/usr/bin/python3.8

# 5. Создайте экземпляры классов, передайте значения атрибутов. Выполните доступ к атрибутам, выведите результат.
# Выполните вызов методов и также покажите результат.


class Some:
    param: int
    _param: int
    __param: int

    def __init__(self, p):
        self.param = p
        self._param = p
        self.__param = p

    def print(self):
        print(self.param, self._param, self.__param)


some = Some(123213)
print(some.param, some._param, some._Some__param)
some.print()

# P.S. И это 5(!) задание?
