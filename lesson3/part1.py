#!/usr/bin/python3.8

# 1. Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление.
# Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.

import re
from typing import Union


def division(x: int, y: int) -> Union[int, str, float]:
    """
    Функция возвращает результат деления двух чисел
    :param x: числитель
    :param y: знаменатель
    :return:  результат
    """
    if y == 0:
        return "Нельзя делить на ноль"
    return x/y


params = input("введите два числа разделяя их пробелом или зяпятой: ")
x, y = re.split(r'[ ,]', params)

print(division(int(x), int(y)))
